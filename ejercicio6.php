<?php

require 'vendor/autoload.php'; // Carga Composer

use GuzzleHttp\Client;

$apiKey = 't7ZDL49dUH9budMeUFD81CSjBzFX3CCDN5Rr9L94';
$client = new Client([
    'verify' => false, 
]);

try {
    // Realiza una solicitud GET a la API de la NASA
    $response = $client->get("https://api.nasa.gov/planetary/apod?api_key=$apiKey");

    // Verifica si la respuesta es exitosa
    if ($response->getStatusCode() == 200) {
        $data = json_decode($response->getBody());

        // Muestra los resultados en HTML de manera entendible
        echo '<h1>' . $data->title . '</h1>';
        echo '<p>' . $data->explanation . '</p>';
        echo '<img src="' . $data->url . '" alt="' . $data->title . '">';
    } else {
        echo 'Error al obtener datos de la API de la NASA.';
    }
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
