<?php

$host = "localhost";
$port = "5432";
$dbname = "ejercicio1";
$user = "postgres";
$password = "postgres";

$dsn = "pgsql:host=$host;port=$port;dbname=$dbname;user=$user;password=$password";

try {
    $db = new PDO($dsn);
} catch (PDOException $e) {
    echo "Error de conexión: " . $e->getMessage();
    die();
}

$query = "SELECT p.nombre AS nombre_producto, p.precio AS precio_producto, m.nombre AS nombre_marca, e.nombre AS nombre_empresa, c.nombre AS nombre_categoria
          FROM public.Producto p
          INNER JOIN public.Marca m ON p.id_marca = m.id_marca
          INNER JOIN public.Empresa e ON m.id_empresa = e.id_empresa
          INNER JOIN public.Categoria c ON p.id_categoria = c.id_categoria
          ORDER BY nombre_producto";

$result = $db->query($query);

echo '<table border="1">';
echo '<tr><th>Nombre Producto</th><th>Precio Producto</th><th>Nombre Marca</th><th>Nombre Empresa</th><th>Nombre Categoría</th></tr>';
foreach ($result as $row) {
    echo '<tr>';
    echo '<td>' . $row['nombre_producto'] . '</td>';
    echo '<td>' . $row['precio_producto'] . '</td>';
    echo '<td>' . $row['nombre_marca'] . '</td>';
    echo '<td>' . $row['nombre_empresa'] . '</td>';
    echo '<td>' . $row['nombre_categoria'] . '</td>';
    echo '</tr>';
}
echo '</table>';

$db = null;
?>
