<?php

$host = "localhost";
$port = "5432";
$dbname = "ejercicio4";
$user = "postgres";
$password = "postgres";

try {
    $conexion = new PDO("pgsql:host=$host;port=$port;dbname=$dbname;user=$user;password=$password");
    echo "Conexión exitosa a la base de datos.";

    for ($i = 1; $i <= 1000; $i++) {
        $nombre = generarCadenaAleatoria(40);
        $descripcion = generarCadenaAleatoria(250);
    
        $stmt = $conexion->prepare("INSERT INTO cadenas (nombre, descripcion) VALUES (:nombre, :descripcion)");
        $stmt->bindParam(':nombre', $nombre);
        $stmt->bindParam(':descripcion', $descripcion);
        $stmt->execute();
    }
    
    echo "Se han insertado 1000 registros en la tabla.";

} catch (PDOException $e) {
    echo "Error de conexión a la base de datos: " . $e->getMessage();
}

function generarCadenaAleatoria($longitud) {
    $caracteres = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $cadenaAleatoria = '';
    for ($i = 0; $i < $longitud; $i++) {
        $cadenaAleatoria .= $caracteres[rand(0, strlen($caracteres) - 1)];
    }
    return $cadenaAleatoria;
}
?>
