<?php

$host = "localhost";
$port = "5432";
$dbname = "ejercicio4";
$user = "postgres";
$password = "postgres";

// Conectarse a la base de datos
$conexion = new PDO("pgsql:host=$host;port=$port;dbname=$dbname;user=$user;password=$password");

// Consulta para obtener los registros
$consulta = $conexion->query("SELECT * FROM cadenas");

echo "<table border='1'>
<tr>
<th>ID</th>
<th>Nombre</th>
<th>Descripción</th>
</tr>";

while ($fila = $consulta->fetch()) {
    echo "<tr>";
    echo "<td>" . $fila['id'] . "</td>";
    echo "<td>" . $fila['nombre'] . "</td>";
    echo "<td>" . $fila['descripcion'] . "</td>";
    echo "</tr>";
}

echo "</table>";
?>
